import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';


import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import New from './pages/New';


class Routes extends React.Component {
    render() {
        /* <Switch>  Apenas uma rota sera executada por vez </Switch> */
        /* <Route path="/" exact component={Login} /> propriedade exact ira chamar a rota 
        apenas se o path for exatamente igual, do contrario o react iria utilizar para todas as rotas que nao existem essa rota  */
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Login} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/new" component={New} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Routes;